package ictgradschool.industry.mock.test02.q2;

import ictgradschool.industry.mock.test02.q2.portfolio.Stock;
import ictgradschool.industry.mock.test02.q2.portfolio.StockPortfolio;
import ictgradschool.industry.mock.test02.q2.portfolio.StockPortfolioListener;

import javax.swing.table.AbstractTableModel;

public class StockTableAdapter extends AbstractTableModel implements StockPortfolioListener {

private StockPortfolio stocks;
final int COMPANY = 0;
final int NUMSHARES = 1;
final int BUYPRICE = 2;
final int CURRENTVAL = 3;

public StockTableAdapter(StockPortfolio stocks){
    this.stocks = stocks;
}
    
    @Override
    public void update(Stock stock) {
    fireTableDataChanged();
    
    }
    
    @Override
    public int getRowCount() {
        return stocks.getNumberOfStocks();
    }
    
    @Override
    public int getColumnCount() {
        return 4;
    }
    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
    Stock current = stocks.getStockAt(rowIndex);
    switch (columnIndex){
        case COMPANY: return current.getCompany();
        case NUMSHARES: return current.getNumberOfShares();
        case BUYPRICE: return current.getBoughtPrice();
        case CURRENTVAL: return current.getCurrentValue();
    }
        return null;
    }
    
    @Override
    public String getColumnName(int columnIndex){
    switch (columnIndex){
        case COMPANY: return "Company name";
        case NUMSHARES: return "Number of shares";
        case BUYPRICE: return "Buy price";
        case CURRENTVAL: return "Current value";
    }
    return "";
    }
}
